# Trabajo Practico - Tecnicas de Diseño - FIUBA

## Extension de usuario: Renderers  

La aplicacion soporta los siguientes tipos de archivo:  
* png  
* gif  
* jpeg  
* webp  
* pdf  

Si el usuario desea puede agregar sus propios visualizadores (Renderers) para soportar mas tipos de archivo haciendo lo siguiente:  

### 1 - Agregar los tipos de archivo que se planea agregar en src/extensions/previews/config.js: 

```js
export const RenderersConfig = {
	//Agrega el path (dentro de esta carpeta) a tu componente
	// de renderizacion y la extension que soporta aqui. Ejemplo:
	mp4: 'VideoPreviewer'
}
``` 

### 2 - Implementar el Renderer:

```js
import React from 'react';
import ReactPlayer from 'react-player'

export default function VideoPreviewer(props) {
	const { file } = props
	return (
		<ReactPlayer url={file} />
	)
}
``` 

## Extension de usuario: Contenido  

Se puede extender la aplicacion para combinar nuestros datos con APIs del usuario o de terceros de la siguiente manera:  

### 1 - Declarar en src/extensions/content/config.js el nuevo contenido a cargar:

```js
export const ContentConfig = [
    //Agrega el path (dentro de esta carpeta) a tu componente
	// de contenido, el titulo y su ruta. Ejemplo:
    /*{
        title: 'Mis Favoritos',
        route: 'favorites',
        componentPath: 'Favorites' 
    },*/
]
```

### 2 - Implementar el componente de contenido:

```js
export default function Favorites() {

    const getFolderData = async (id) => {
        const res = await api.get(`folders/` + id);
        console.log(res.data)
    }

    useEffect(() => {
        getFolderData("")
    }, [])

    return(
        <div>
            <NavigationBar/>
            <div style={{marginLeft: '280px'}}>
                <h1>Ejemplo de componente agregado por usuario</h1>
            </div>
        </div>
    )
}
```
