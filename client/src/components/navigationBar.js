import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PeopleIcon from '@material-ui/icons/People';
import StorageIcon from '@material-ui/icons/Storage';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {useHistory} from "react-router";
import { ContentConfig } from '../extensions/content/config'

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
}));


export default function NavigationBar() {
  const history = useHistory();
  const classes = useStyles();
  
  function logout(){
    localStorage.removeItem('token');
    history.push('/login')
  }
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" noWrap>
            Gugel Draib
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
      >
        <div className={classes.toolbar} />
        <Divider />
        <List>
            <ListItem button key="Mi unidad" onClick={() => history.push('/')}>
                <ListItemIcon><StorageIcon/></ListItemIcon>
                <ListItemText primary="Mi unidad" />
            </ListItem>
            <ListItem button key="Compartido conmigo" onClick={() => history.push('/sharedWithMe')}>
                <ListItemIcon><PeopleIcon/></ListItemIcon>
                <ListItemText primary="Compartido conmigo" />
            </ListItem>
            {ContentConfig.map((content) => (
              <ListItem 
                button 
                key={content.title} 
                onClick={() => history.push(`/${content.route}`)}
              >
                <ListItemIcon><FavoriteIcon/></ListItemIcon>
                <ListItemText primary={content.title} />
              </ListItem>
            ))}
        </List>
        <Divider />
          <List>  
            <ListItem button key="Cerrar Sesión" onClick={logout}>
              <ListItemIcon><ExitToAppIcon/></ListItemIcon>
              <ListItemText primary="Cerrar Sesión" />
            </ListItem>
          </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        
      </main>
    </div>
  );
}