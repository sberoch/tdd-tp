import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { useHistory, useLocation } from "react-router";
import api from '../network/api'

export default function UploadFileForm(props) {
  const history = useHistory();
  const location = useLocation();
  const [file, setFile] = useState()
  const folder = location.folder

  const handleFileSelection = (e) => {
    console.log(e.target.files[0])
    setFile(e.target.files[0])
  }

  const handleUpload = async () => {

    let formData = new FormData()
    formData.append("fileName", file.name)
    formData.append("parentId", folder ? folder._id : '')
    formData.append("file", file)
    formData.append("userId", localStorage.getItem("token"))

    console.log(formData)

    try {
      await api.post(`medias/`, formData, {
        headers: {
          'content-type': 'multipart/form-data'
        }
      })
      history.push('/')
    } catch {
      console.log("Error")
    }
  }

  return (
    <Grid container spacing={3} justify="center">
      <Grid item xs={10}>
        El archivo sera ubicado en {folder ? folder.name : 'root'}
      </Grid>
      <Grid item xs={10}>
        <Button
          variant="outlined"
          component="label"
          fullWidth
        >
          {file ? file.name : 'Selecciona un archivo...'} 
          <input
            type="file"
            hidden
            onChange={handleFileSelection}
          />
        </Button>
      </Grid>
      <Grid item xs={10} style={{margin: '40px'}}>
        <Button 
          size="large"
          color="primary" 
          variant="contained" 
          fullWidth
          onClick={handleUpload} 
        >
        Subir
        </Button>
      </Grid>
    </Grid>
  )
}