import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import PersonIcon from '@material-ui/icons/Person';
import api from '../network/api'
import Loading from './Loading';
import BottomMessage from './BottomMessage';
import { makeStyles } from '@material-ui/core/styles';
import { blue, green } from '@material-ui/core/colors';
import Divider from '@material-ui/core/Divider';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles({
  avatar: {
    backgroundColor: green[100],
    color: "#004b23",
  },
  addPerson:{
    backgroundColor: "#004b23",
    color: green[0]
  }
});
export default function SimpleDialog(props) {
  const classes = useStyles();
  const { onClose, open, sharingFileId } = props;
  const [userId, setUserId] = useState("")
  const [loading, setLoading] = useState(false)
  const [showMessage, setShowMessage] = useState(false)
  const [message, setMessage] = useState("")
  const [disableButton, setDisableButton] = useState(false)
  const [sharingUsers, setSharingUsers] = useState([])

//   const handleClose = () => {
//     onClose(selectedValue);
//   };

  const handleChange = (e) => {
    setUserId(e.target.value)
  };

  const handleDeleteSharingUser = async (permissionId, email) => {
    try {
      const res = await api.delete(`/permissions/${permissionId}`)
      console.log(res.data)
      setMessage("Se ha eliminado el acceso a "+email)
      setSharingUsers(sharingUsers.filter(user => user.id == email))
    } catch {
      setMessage("No se pudo eliminar el acceso a "+email)
    }
    setShowMessage(true)
  }

  const handleShare = async () => {
    setDisableButton(true)
    setLoading(true);
    let formData = {
        "mediaId":sharingFileId,
        "userId":userId,
        "code":0
    }
    try {
      await api.post(`permissions/`,formData)
      setMessage("El archivo se compartió correctamente")
    }
    catch{
      setMessage("No se pudo compartir el archivo")
    }
    setDisableButton(false)
    setShowMessage(true)
    setLoading(false);
    onClose()
  };

  const getSharingUsers = async () => {
    const res = await api.get(`permissions/medias/`+sharingFileId+`/users`);
    console.log(res.data)
    setSharingUsers(res.data)
  }
  useEffect(() => {
    if (sharingFileId) {
      getSharingUsers()
    }
  }, [sharingFileId])

  return (
    <div>

    <Dialog onClose={onClose} aria-labelledby="simple-dialog-title" open={open}>
      <Loading open = {loading} />
      <DialogTitle id="simple-dialog-title">
      <div style={{"display":"flex"}}>

      <Avatar className={classes.addPerson}>
        <PersonAddIcon/>
      </Avatar>
      <div style={{"marginLeft":"10px","marginTop":"3px"}}>
        Compartir archivo

      </div>
      </div>
      </DialogTitle>
          <Divider  />
      <DialogContent>
          <List>
        {sharingUsers.map((user) => (
          <ListItem key={user.userId}>
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                <PersonIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={user.userId} />
            <IconButton edge="end" aria-label="share" onClick={() =>{handleDeleteSharingUser(user.permissionId,user.userId)}}>
                  <DeleteIcon />
                </IconButton>
          </ListItem>
        ))}

        
      </List>
      {
        sharingUsers.length > 0 && <Divider  />

      }
      <TextField
        autoFocus
        margin="dense"
        id="name"
        label="Email Address"
        type="email"
        onChange={handleChange}
        fullWidth
        style={{"width":"300px"}}
        />
      </DialogContent>

        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={handleShare} color="primary" disabled={disableButton}>
            Compartir
          </Button>
        </DialogActions>
        
    </Dialog>
        <BottomMessage open={showMessage} setOpen={setShowMessage} message={message} />
    </div>

  );
}
