import React, { useState, useEffect } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Snackbar from '@material-ui/core/Snackbar';
import FolderIcon from '@material-ui/icons/Folder';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import AddIcon from '@material-ui/icons/Add';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import DeleteIcon from '@material-ui/icons/Delete';
import Preview from './preview/Preview'
import SharingFileDialog from '././SharingFileDialog';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router";
import api from '../network/api';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

export default function FolderContent(props) {
  const history = useHistory();
  const classes = useStyles();
  const [dense] = useState(false);
  const [previewOpen, setPreviewOpen] = useState(false);
  const [previewedFile, setPreviewedFile] = useState("");
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [folders, setFolders] = useState(props.folderChildren)
  const [medias, setMedias] = useState(props.mediaChildren)
  const [sharingFileId, setSharingFileId] = useState("")

  const [sharingFile, setSharingFile] = useState(false);

  useEffect(() => {
    setFolders(props.folderChildren)
    setMedias(props.mediaChildren)
  }, [props])

  const handlePreviewOpen = (media) => {
    setPreviewedFile(media._id)
    setPreviewOpen(true)
  }
  const handlePreviewClose = () => {
    setPreviewOpen(false)
  }
  const handleSnackbarClose = () => {
    setOpenSnackbar(false)
  }
  const handleNewFile = () => {
    history.push({
      pathname: '/uploadFile',
      folder: props.currentFolder ? props.currentFolder : undefined
    })
  }
  const handleNewFolder = () => {
    history.push({
      pathname: '/uploadFolder',
      folder: props.currentFolder ? props.currentFolder : undefined
    })
  }
  const handleDeleteFolder = async (folderId) => {
    try {
      const res = await api.delete(`/folders/${folderId}`)
      console.log(res.data)
      setOpenSnackbar(true)
      props.setFolders(folders.filter((folder) => folder._id !== folderId)) 
    } catch {
      console.log("Error deleting folder")
    }
  }
  const handleDeleteFile = async (fileId) => {
    try {
      const res = await api.delete(`/medias/${fileId}`)
      console.log(res.data)
      props.setMedias(medias.filter((media) => media._id !== fileId)) 
      setOpenSnackbar(true)
    } catch {
      console.log("Error deleting file")
    }
  }

  return(
    <div className={classes.demoO}>
      {
      <SharingFileDialog onClose={()=>setSharingFile(false)} open={sharingFile} sharingFileId = {sharingFileId}/>
      }
      <Preview 
        fileId={previewedFile}
        isOpen={previewOpen} 
        handlePreviewClose={handlePreviewClose} />
      <List dense={dense}>
        {folders && folders.map((folder) => {
          return(
            <ListItem button onClick={(e) => props.openFolder(folder._id,e)} key={folder._id}>
              <ListItemAvatar>
                <Avatar>
                  <FolderIcon style={{fill: "#F1C511"}}/>
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={folder.name}
              />
              <ListItemSecondaryAction>
                <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteFolder(folder._id)}>
                  <DeleteIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
        )})}
        {medias && medias.map((media) => {
          return(
            <ListItem button key={media._id} onClick={(e) => handlePreviewOpen(media)}>
              <ListItemAvatar>
                <Avatar>
                  <InsertDriveFileIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={media.name}
              />
              {
                props.isHome &&
              <ListItemSecondaryAction>
                <IconButton edge="end" aria-label="share" onClick={() => {setSharingFile(true);setSharingFileId(media._id)}}>
                  <PersonAddIcon />
                </IconButton>

                <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteFile(media._id)}>
                  <DeleteIcon />
                </IconButton>
              </ListItemSecondaryAction>
              }
            </ListItem>
        )})}
        {props.isHome && <ListItem button key="nueva_carpeta" onClick={handleNewFolder}>
          <ListItemAvatar>
            <Avatar>
              <AddIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Nueva carpeta"
          />
        </ListItem>}
        {props.isHome && <ListItem button key="nuevo_archivo" onClick={handleNewFile}>
          <ListItemAvatar>
            <Avatar>
              <AddIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary="Nuevo archivo"
          />
        </ListItem>}
      </List>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={openSnackbar}
        autoHideDuration={6000}
        onClose={handleSnackbarClose}
        message="Se elimino con exito"
      />
    </div>
    )
}