import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { useHistory, useLocation } from "react-router";
import api from '../network/api'

export default function UploadFolderForm(props) {
  const history = useHistory();
  const location = useLocation();
  const [folderName, setFolderName] = useState('')
  const parentfolder = location.folder

  const handleUpload = async () => {
    const folder = {
      folderName: folderName,
      parentId: parentfolder ? parentfolder._id : '',
      userId: localStorage.getItem("token")
    }
    try {
      await api.post(`folders/`, folder)
      history.push('/')
    } catch {
      console.log("Error")
    }
  }

  const handleChange = (e) => {
    setFolderName(e.target.value)
  }

  return (
    <Grid container spacing={3} justify="center">
      <Grid item xs={10}>
        La carpeta sera ubicada en {parentfolder ? parentfolder.name : 'root'}
      </Grid>
      <Grid item xs={10}>
        <TextField
          fullWidth
          variant="outlined"
          id="folder"
          label="Nombre de carpeta"
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={10} style={{margin: '40px'}}>
        <Button 
          size="large"
          color="primary" 
          variant="contained" 
          fullWidth
          onClick={handleUpload} 
        >
        Subir
        </Button>
      </Grid>
    </Grid>
  )
}