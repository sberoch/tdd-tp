import React, { useState } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { Link } from "react-router-dom";
import { useHistory } from "react-router";
import api from '../../network/api'

const validationSchema = yup.object({
  email: yup
    .string('Ingresa tu email')
    .email('Ingresa un email valido')
    .required('Se requiere un email'),
  name: yup
    .string('Ingresa tu nombre')
    .required('Se requiere tu nombre'),
  lastname: yup
    .string('Ingresa el nombre de tu negocio')
    .required('Se requiere el nombre de tu negocio'),
  birthdate: yup
    .string('Ingresa tu fecha de nacimiento')
    .required('Se requiere fecha de nacimiento'),
  password: yup
    .string('Ingresa tu contaseña')
    .required('Se requiere una contaseña'),
});

export default function SignupForm() {
  const formik = useFormik({
    initialValues: {
      email: '',
      name: '',
      lastname: '',
      birthdate: '',
      password: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSignup(values)
    },
  });

  const history = useHistory();
  const [avatar, setAvatar] = useState();

  const handleFileSelection = (e) => {
    setAvatar(e.target.files[0])
  }

  const handleSignup = async (user) => {
    let formData = new FormData()
    formData.append("email", user.email)
    formData.append("name", user.name)
    formData.append("lastname", user.lastname)
    formData.append("birthdate", user.birthdate)
    formData.append("password", user.password)
    formData.append("avatar", avatar, avatar.name)
    console.log(formData)

    try {
      const res = await api.post(`api/auth/register`, formData, {
        headers: {
          'content-type': 'multipart/form-data'
        }
      })
      console.log(res)
      localStorage.setItem("token", res.data.accessToken)
      history.push('/')
    } catch {
      console.log("Error")
    }
  }
  
  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container spacing={3} justify="center">
        <Grid item xs={10} align="center">
          <Typography variant="h2" color="primary">Gugel Draib</Typography>
        </Grid>
        <Grid item xs={10}>
          <TextField
            fullWidth
            variant="outlined"
            id="email"
            name="email"
            label="Email"
            value={formik.values.email}
            onChange={formik.handleChange}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
        </Grid>
        <Grid item xs={10}>
          <TextField
            fullWidth
            variant="outlined"
            id="name"
            name="name"
            label="Nombre"
            value={formik.values.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.touched.name && formik.errors.name}
          />
        </Grid>
        <Grid item xs={10}>
          <TextField
            fullWidth
            variant="outlined"
            id="lastname"
            name="lastname"
            label="Apellido"
            value={formik.values.lastname}
            onChange={formik.handleChange}
            error={formik.touched.lastname && Boolean(formik.errors.lastname)}
            helperText={formik.touched.lastname && formik.errors.lastname}
          />
        </Grid>
        <Grid item xs={10}>
          <TextField
            fullWidth
            variant="outlined"
            id="birthdate"
            name="birthdate"
            label="Fecha de nacimiento"
            value={formik.values.birthdate}
            onChange={formik.handleChange}
            error={formik.touched.birthdate && Boolean(formik.errors.birthdate)}
            helperText={formik.touched.birthdate && formik.errors.birthdate}
          />
        </Grid>
        <Grid item xs={10}>
          <TextField
            fullWidth
            variant="outlined"
            id="password"
            name="password"
            label="Contraseña"
            type="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
          />
        </Grid>
        {avatar && <Grid item xs={10} style={{textAlign: "center"}}>
          <img src={URL.createObjectURL(avatar)} width="200" height="200" alt="" />
        </Grid>}
        <Grid item xs={10}>
          <Button
            variant="outlined"
            component="label"
            fullWidth
          >
            {avatar ? avatar.name : 'Selecciona tu foto de perfil'} 
            <input
              type="file"
              hidden
              onChange={handleFileSelection}
            />
          </Button>
        </Grid>
        <Grid item xs={10}>
          <Button 
            size="large"
            color="primary" 
            variant="contained" 
            fullWidth 
            type="submit"
          >
          Registrarme
          </Button>
        </Grid>
        <Grid item xs={10}>
          <Link to='/login'>
            <Button color="primary" style={{textTransform: 'none'}}>
              ¿Ya tenes una cuenta? Ingresar
            </Button>
          </Link>
        </Grid>
      </Grid>
    </form>
  )
}