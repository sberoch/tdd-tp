import React from 'react';


import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';


export default function FolderPath(props) {
    return(
        <Breadcrumbs aria-label="breadcrumb">
            {props.currentFolder && props.currentFolder.path.map((folder) => {
                return(
                    <Link color="inherit" style={{'cursor':'pointer'}}onClick={(e) => props.openFolder(folder.id,e)}>
                        {folder.name}
                    </Link>

                )})}
            {props.currentFolder && <Link 
                color="textPrimary"
                style={{'cursor':'pointer'}}
                onClick={(e) => props.openFolder(props.currentFolder._id,e)}
                aria-current="page"
                >
                    {props.currentFolder.name}
                </Link>
            }
        </Breadcrumbs>
    )
}