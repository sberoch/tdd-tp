import React from 'react';

export default function ImagePreview(props) {
	const { file } = props
	const extension = file.metadata.name.split('.').pop();
	return (
		<img src={`data:image/${extension};base64,${file.file}`} alt={file.metadata.name}/>
	)
}