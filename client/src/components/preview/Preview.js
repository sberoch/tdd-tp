import React, { useState, useEffect, Suspense } from 'react'
import Backdrop from '@material-ui/core/Backdrop';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import GetAppIcon from '@material-ui/icons/GetApp';
import CloseSharpIcon from '@material-ui/icons/CloseSharp';
import { makeStyles } from '@material-ui/core/styles';
import ImagePreview from "./ImagePreview"
import PDFPreview from "./PDFPreview"

import { RenderersConfig } from '../../extensions/previews/config.js'
import api from '../../network/api';

function loadComponent(name) {
  const Component = React.lazy(() =>
    import(`../../extensions/previews/${name}.js`)
  );
  return Component;
}

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

export default function Preview(props) {
  const classes = useStyles();
  const { fileId, isOpen, handlePreviewClose } = props;
  const [file, setFile] = useState()

  useEffect(() => {
    async function fetchFile() {
      const res = await api.get(`medias/${fileId}`)
      console.log(res.data)
      setFile(res.data)
    }
    if (props.isOpen){
      fetchFile()
    }
  }, [props.isOpen])

  const handleClose = () => {
    handlePreviewClose()
  };

  const handleDownload = () => {
    const extension = file.metadata.name.split('.').pop();
    const link = document.createElement('a')
    link.href = `data:application/${extension};base64,${file.file}`;
    link.setAttribute(
      'download',
      `${file.metadata.name}`,
    )
    document.body.appendChild(link)
    link.click()
    link.parentNode.removeChild(link)
  }

  const renderPreviewComponent = () => {
    console.log(file)
    const extension = file ? file.metadata.name.split('.').pop() : '';

    //For used defined renderers
    for (var ext in RenderersConfig) {
      if (extension === ext) {
        var Component = loadComponent(RenderersConfig[extension])
        return <Component file={file}/>
      }
    }
    switch (extension) {
      case 'png':
      case 'gif':
      case 'jpeg':
      case 'jpg':
      case 'webp':
        return <ImagePreview file={file} />
      case 'pdf':
        return <PDFPreview file={file} />
      default:
        return <Typography variant="h4">No se pudo previsualizar el archivo.</Typography>
    }
  }

  return (
    <div>
      <Backdrop className={classes.backdrop} open={props.isOpen} onClick={handleClose}>
        <Box style={{ position: "fixed", right: 50, top: 10}} >
          <IconButton onClick={handleDownload}>
            <GetAppIcon 
              style={{fill: "white", width: 40, height:40}}/>
          </IconButton>
          <IconButton onClick={handleClose}>
            <CloseSharpIcon 
              style={{fill: "white", width: 40, height:40}}/>
          </IconButton>
        </Box>
        {file && 
          <Box width="auto">
            <Suspense fallback={<div>Loading...</div>}> 
              {renderPreviewComponent()}
            </Suspense>
          </Box>
        }
      </Backdrop>
    </div>
  );
}
