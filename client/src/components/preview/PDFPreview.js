import React from 'react';
import { Document, Page } from 'react-pdf';
import { pdfjs } from 'react-pdf';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.min.js`;

export default function PDFPreview(props) {
	const { file } = props
	return (
		<Document file={`data:application/pdf;base64,${file.file}`}>
	      <Page pageNumber={1} height={400}/>
	    </Document>
	)
}