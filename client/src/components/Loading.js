import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';


export default function Loading(props) {

  const { open } = props;

  return (
    <div>
      { open &&
          <CircularProgress style={{'marginLeft':'150px', 'marginTop':'40px'}} />
      }  
    </div>
  );
}