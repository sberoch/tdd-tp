import React from 'react';
import LoginForm from '../components/auth/LoginForm'

export default function Login() {
    return (
        <div style={{padding: 80}}>
        	<LoginForm />
        </div>
    )
}