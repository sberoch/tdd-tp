import React from 'react';
import UploadFolderForm from '../components/UploadFolderForm'
import NavigationBar from '../components/navigationBar';

export default function UploadFolder() {
    return (
        <div style={{padding: 100}}>
            <NavigationBar/>
            <div style={{marginLeft: '240px'}}>
        	    <UploadFolderForm />
            </div>
        </div>
    )
}