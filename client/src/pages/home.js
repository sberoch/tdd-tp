import React, { useEffect, useState } from 'react';
import NavigationBar from '../components/navigationBar';
import api from '../network/api'

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/core/styles';
import FolderContent from '../components/FolderContent';
import FolderPath from '../components/FolderPath';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

export default function Home() {
  const classes = useStyles();
  let [mediaChildren, setMediaChildren] = useState([]);
  let [folderChildren, setFolderChildren] = useState([]);
  let [currentFolder, setCurrentFolder] = useState();

  const openFolder = async (id) => {
    const res = await api.get(`folders/` + id);
    setMediaChildren(res.data.media_childs)
    setFolderChildren(res.data.folder_childs)
    setCurrentFolder(res.data.folder)
  }

  useEffect(() => {
    openFolder("");
  },[])

  return (
    <div>
      <NavigationBar/>
      <Grid item xs={12} md={6} style={{'marginLeft':'290px'}}>
        <Typography variant="h6" className={classes.title}>
            Mi unidad
        </Typography>
        <FolderPath currentFolder={currentFolder} openFolder={openFolder} />
        <FolderContent 
          currentFolder={currentFolder}
          folderChildren={folderChildren} 
          mediaChildren={mediaChildren}
          setMedias={setMediaChildren}
          setFolders={setFolderChildren}
          openFolder={openFolder}
          isHome = {true}
        />
      </Grid>
    </div>
  )
}