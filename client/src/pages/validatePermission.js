import React , { useEffect } from 'react';
import api from '../network/api'
import { useHistory } from "react-router";
import {useLocation} from 'react-router-dom';

export default function ValidatePermission(props) {
    const history = useHistory();
    const {search} = useLocation()

    const sendValidation = async (idValidation,token) => {
        await api.put(`permissions/`+idValidation+'/validar', {"token": token});
      }

    useEffect(() => {
        // sendValidation()
        const params = new URLSearchParams(search)
        let token = params.get('token')
        let idValidation = params.get('id')
        sendValidation(idValidation,token)
        history.push('/sharedWithMe')
      }, [props, search, history])

    return(
        <div>
            Procesando.. 
            {/* {location.id} y {location.token} */}
        </div>
    )   
}