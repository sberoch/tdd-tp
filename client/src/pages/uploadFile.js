import React from 'react';
import UploadFileForm from '../components/UploadFileForm'
import NavigationBar from '../components/navigationBar';

export default function UploadFolder() {
    return (
        <div style={{padding: 100}}>
            <NavigationBar/>
            <div style={{marginLeft: '240px'}}>
        	    <UploadFileForm />
            </div>
        </div>
    )
}