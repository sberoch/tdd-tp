import React, { useEffect, useState } from 'react';
import NavigationBar from '../components/navigationBar';
import api from '../network/api'

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/core/styles';
import FolderContent from '../components/FolderContent';



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

export default function Home() {
  const classes = useStyles();
  let [mediaChildren, setMediaChildren] = useState([]);
  // let [folderChildren, setFolderChildren] = useState([]);
  // let [currentFolder, setCurrentFolder] = useState();

  const openFolder = async () => {
    const res = await api.get(`permissions/files/`);
    setMediaChildren(res.data)
    // setFolderChildren(res.data.folder_childs)
    // setCurrentFolder(res.data.folder)
  }

  useEffect(() => {
    // openFolder("");
    openFolder("")
  },[])

  return (
    <div>
      <NavigationBar/>
      <Grid item xs={12} md={6} style={{'marginLeft':'290px'}}>
        <Typography variant="h6" className={classes.title}>
            Compartido conmigo
        </Typography>
        {/* <FolderPath currentFolder={currentFolder} openFolder={openFolder} /> */}
        <FolderContent 
          // currentFolder={currentFolder}
          // folderChildren={folderChildren} 
          mediaChildren={mediaChildren} 
          openFolder={openFolder}
          isHome = {false}
        />
      </Grid>
    </div>
  )
}