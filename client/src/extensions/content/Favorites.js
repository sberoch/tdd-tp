import React, {useEffect} from 'react';
import NavigationBar from '../../components/navigationBar';
import api from '../../network/api'


export default function Favorites() {

    const getFolderData = async (id) => {
        const res = await api.get(`folders/` + id);
        console.log(res.data)
    }

    useEffect(() => {
        getFolderData("")
    }, [])

    return(
        <div>
            <NavigationBar/>
            <div style={{marginLeft: '280px'}}>
                <h1>Ejemplo de componente agregado por usuario</h1>
            </div>
        </div>
    )
}