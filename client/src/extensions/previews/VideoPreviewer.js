import React from 'react';
import ReactPlayer from 'react-player'

export default function VideoPreviewer(props) {
	const { file } = props
	return (
		<ReactPlayer url={file} />
	)
}

