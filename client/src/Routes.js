import React, {Suspense} from 'react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import {BrowserRouter as Router, Route, Redirect, Switch} from "react-router-dom";
import Login from "./pages/login"
import Signup from "./pages/signup"
import UploadFolder from "./pages/uploadFolder"
import UploadFile from "./pages/uploadFile"
import Home from "./pages/home"
import SharedWithMe from "./pages/sharedWithMe"
import { ContentConfig } from './extensions/content/config'
import ValidatePermission from './pages/validatePermission';

function loadComponent(name) {
    const Component = React.lazy(() =>
        import(`./extensions/content/${name}.js`)
    );
    return Component;
}

const theme = createMuiTheme({
    typography: {
      fontFamily: [
          'Nunito',
      ].join(','),
    },
    palette: {
        primary: {
          main: '#004b23',
        },
        secondary: {
          main: '#f44336',
        },
      },
  });

class Routes extends React.Component {

    componentDidMount(){
        document.title = "Gugel Draib"
    }

    render() {
        return (
            <ThemeProvider theme={theme}>
                <Router>
                    <main className="main-div">
                        <Switch>
                            <PrivateRoute path="/" exact component= {props => <Home/>} />
                            {ContentConfig.map((content) => {
                                var Component = loadComponent(content.componentPath)
                                return(
                                    <Suspense fallback={<div>Loading...</div>}>
                                        <PrivateRoute path={"/" + content.route} exact component= {props => <Component/>} />
                                    </Suspense>
                            )})}
                            <PrivateRoute path="/sharedWithMe" component={props => <SharedWithMe/>} />
                            <PrivateRoute path="/uploadFolder" component={props => <UploadFolder/>} />
                            <PrivateRoute path="/uploadFile" component={props => <UploadFile/>} />
                            <PrivateRoute path="/permissions/validate" component={props => <ValidatePermission/>}/>
                            <Route path="/login" component= {props => <Login/>} />
                            <Route path="/signup" component= {props => <Signup/>} />
                        </Switch>
                    </main>
                </Router>
            </ThemeProvider>
        )
    }
}

export default Routes;

const PrivateRoute = ({ component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            localStorage.getItem("token") ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/login",
                        state: { from: props.location }
                    }}
                />
            )
        }
    />
)