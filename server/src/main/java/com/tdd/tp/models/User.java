package com.tdd.tp.models;

import lombok.Data;
import lombok.NonNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.InputStream;

@Data
@Document(collection = "users")
public class User {

	@Id	@NonNull private String email;
	@NonNull private String name;
	@NonNull private String lastname;
	@NonNull private String birthdate;
	@NonNull private String password;
	@NonNull private String username;
	private InputStream avatar;

	public String getEmail(){
    return this.email;
  }
}