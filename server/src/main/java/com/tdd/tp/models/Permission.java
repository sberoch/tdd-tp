package com.tdd.tp.models;

import lombok.Data;
import lombok.NonNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "permissions")
public class Permission {
	private String _id;
	@NonNull private String mediaId;
	@NonNull private String userId;
	@NonNull private String code;
	@NonNull private String token;
	@NonNull private String validate;

	static public String VALIDATED = "1";
	static public String NOT_VALIDATED = "0";

	public String get_id(){
    return this._id;
  }

  public String getMediaId(){
    return this.mediaId;
  }

  public String getUserId(){
    return this.userId;
  }

  public String getCode(){
    return this.code;
  }

  public String getToken(){
    return this.token;
  }

  public String getValidate(){
    return this.validate;
  }

  public void setValidate(String validate){
    this.validate = validate;
  }
}