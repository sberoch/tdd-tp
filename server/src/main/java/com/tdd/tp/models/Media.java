package com.tdd.tp.models;

import lombok.Data;
import lombok.NonNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.InputStream;
import org.springframework.web.multipart.MultipartFile;

@Data
@Document(collection = "medias")
public class Media {

	private String _id;
	@NonNull private String name;
	@NonNull private String parentId;
	@NonNull private String userId;
    private InputStream file;

	public String get_id(){
        return this._id;
    }
    public String getName(){
        return name;
    }
    public String getParentId(){
    	return parentId;
    }

    public String getUserId(){
        return userId;
    }

    public InputStream getFile(){
        return file;
    }

    public void setFile(InputStream archivo){
        this.file = archivo;
    }
}
