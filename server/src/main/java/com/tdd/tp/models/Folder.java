package com.tdd.tp.models;

import lombok.Data;
import lombok.NonNull;
import java.util.List;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "folders")
public class Folder {
    private String _id;
    @NonNull private String name;
    @NonNull private String parentId;
    @NonNull private List path;
    @NonNull private String userId;

    public List getPath(){
        return this.path;
    }
    public String get_id(){
        return this._id;
    }
    public String getName(){
        return name;
    }
    public String getUserId(){
        return userId;
    }
}