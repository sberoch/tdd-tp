package com.tdd.tp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import org.springframework.beans.factory.annotation.Autowired;
import com.tdd.tp.repositories.PermissionRepository;
import com.tdd.tp.repositories.MediaRepository;
import com.tdd.tp.repositories.UserRepository;
import com.tdd.tp.models.Permission;
import com.tdd.tp.models.Media;
import com.tdd.tp.models.User;
import com.tdd.tp.utils.RandomString;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.HashMap;
import java.util.Date;
import java.sql.Timestamp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.mail.javamail.JavaMailSender;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import javax.validation.Valid;
import com.tdd.tp.security.JwtUtils;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
public class PermissionController {

	@Autowired
	PermissionRepository permissionRepository;

	@Autowired
	MediaRepository mediaRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
  private JavaMailSender sender;

  @Autowired
	JwtUtils jwtUtils;

	@RequestMapping(value={"/permissions"}, method = RequestMethod.GET)
	public ResponseEntity getPermissions(){
		try
		{
			List listOfPermissions = new ArrayList<>();
			permissionRepository.findAll().forEach(listOfPermissions::add);
			return new ResponseEntity<>(listOfPermissions, HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/permissions/{id}"}, method = RequestMethod.GET)
	public ResponseEntity getPermissions(@PathVariable String id){
		try
		{
			Permission permission = permissionRepository.findById(id).get();
			return new ResponseEntity<>(permission, HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/permissions/{id}/validar"}, method = RequestMethod.PUT)
	public ResponseEntity postPermissions(@PathVariable String id, @Valid @RequestBody UploadPermissionValidator uploadPermissionValidator) {
		try {
			if(!permissionRepository.findById(id).isEmpty()){
				Permission permission = permissionRepository.findById(id).get();
				if(permission.getToken().equals(uploadPermissionValidator.getToken())){
					if(permission.getValidate().equals(Permission.NOT_VALIDATED)){
						permission.setValidate(Permission.VALIDATED);
						permissionRepository.save(permission);
						return new ResponseEntity<>(permission, HttpStatus.OK);
					}
					else{
						return new ResponseEntity<>(ResponseError("/permissions/" + id + "/validar", 400, "El permiso ya fue validado"), HttpStatus.BAD_REQUEST);
					}
				}else
					return new ResponseEntity<>(ResponseError("/permissions/" + id + "/validar", 400, "Token incorrecto"), HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(ResponseError("/permissions/" + id + "/validar", 400, "El permiso no existe"), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/permissions/files"}, method = RequestMethod.GET)
	public ResponseEntity getFilesPermissionsByUser(@RequestHeader("Authorization") String token){
		try
		{
			String userId = jwtUtils.getUserNameFromJwtToken((token.split(" "))[1]);
			List listFiles = new ArrayList<>();
			permissionRepository.findByUserIdAndValidate(userId, Permission.VALIDATED).forEach( element -> {
				Permission permission = (Permission) element;
				HashMap<String, String> file = new HashMap<String, String>();
				try{
					Media media = mediaRepository.findById(permission.getMediaId()).get();
					file.put("_id", media.get_id());
					file.put("name", media.getName());
					listFiles.add(file);

				}
				catch (Exception e){
					System.out.println(e);
				}
			});
			return new ResponseEntity<>(listFiles, HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/permissions/medias/{mediaId}/users"}, method = RequestMethod.GET)
	public ResponseEntity postPermissions(@PathVariable String mediaId) {
		try {
			
			List listUsers = new ArrayList<>();
			permissionRepository.findByMediaIdAndValidate(mediaId, Permission.VALIDATED).forEach( element -> {
				Permission permission = (Permission) element;
				HashMap<String, String> userMap = new HashMap<String, String>();
				User user = userRepository.findById(permission.getUserId()).get();
				userMap.put("userId", user.getEmail());
				userMap.put("name", user.getName());
				userMap.put("mediaId", permission.getMediaId());
				userMap.put("code", permission.getCode());
				userMap.put("validate", permission.getValidate());
				userMap.put("permissionId", permission.get_id());
				listUsers.add(userMap);
			});
			return new ResponseEntity<>(listUsers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/permissions"}, method = RequestMethod.POST)
	public ResponseEntity postPermissions(@Valid @RequestBody UploadPermissionRequest uploadPermissionRequest) {
		try {
			if(permissionRepository.existsByUserIdAndMediaIdAndValidate(
				uploadPermissionRequest.getUserId(),
				uploadPermissionRequest.getMediaId(),
				Permission.VALIDATED
			)){
				return new ResponseEntity<>(ResponseError("/permissions", 400, "El archivo fue compartido con el usuario y validado"), HttpStatus.BAD_REQUEST);
			}
			
			Permission permission = new Permission(
				uploadPermissionRequest.getMediaId(),
				uploadPermissionRequest.getUserId(),
				uploadPermissionRequest.getCode(),
				RandomString.getAlphaNumericString(24),
				Permission.NOT_VALIDATED
			);

			Permission createdPermission = permissionRepository.save(permission);
			User userToShare = userRepository.findById(createdPermission.getUserId()).get();
			sendEmail(userToShare.getEmail(), createdPermission);
			return new ResponseEntity<>(permission, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value={"/permissions/validate"}, method = RequestMethod.POST)
	public ResponseEntity postPermissionsValidate(@Valid @RequestBody UploadPermissionRequest uploadPermissionRequest) {
		try {
			if(permissionRepository.existsByUserIdAndMediaIdAndValidate(
				uploadPermissionRequest.getUserId(),
				uploadPermissionRequest.getMediaId(),
				Permission.VALIDATED
			)){
				return new ResponseEntity<>(ResponseError("/permissions", 400, "El archivo fue compartido con el usuario y validado"), HttpStatus.BAD_REQUEST);
			}
			Permission permission = new Permission(
				uploadPermissionRequest.getMediaId(),
				uploadPermissionRequest.getUserId(),
				uploadPermissionRequest.getCode(),
				RandomString.getAlphaNumericString(24),
				Permission.VALIDATED
			);

			Permission createdPermission = permissionRepository.save(permission);
			return new ResponseEntity<>(createdPermission, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/permissions/{id}"}, method = RequestMethod.DELETE)
	public ResponseEntity deletePermission(@PathVariable String id){
		try
		{
			Permission permission = permissionRepository.findById(id).get();
			permissionRepository.findByUserIdAndMediaId(permission.getUserId(), permission.getMediaId()).forEach( element -> {
				Permission permissionElement = (Permission) element;
				permissionRepository.deleteById(permissionElement.get_id());
			});

			permissionRepository.deleteById(id);
			return new ResponseEntity<>(permission, HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/permissions/deleteall"}, method = RequestMethod.DELETE)
	public ResponseEntity deletePermissions(){
		try
		{
			permissionRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private void sendEmail(String mail, Permission permission) throws Exception{
    MimeMessage message = sender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true);
    helper.setTo(mail);
    helper.setText("<html><body><a href=http://127.0.0.1:3000/permissions/validate?id=" + permission.get_id() + "&token=" + permission.getToken() + ">Validar</a></body></html>", true);
    helper.setSubject("Hi");
    sender.send(message);
  }

  private HashMap<String, String> ResponseError(String path, int status, String errorMessage){
  	HashMap<String, String> response = new HashMap<String, String>();
		response.put("timestamp", (new Timestamp((new Date()).getTime())).toString());
		response.put("status", String.valueOf(status));
		response.put("error", "Bad Request");
		response.put("defaultMessage", errorMessage);
		response.put("path", path);
		return response;
  }
}
