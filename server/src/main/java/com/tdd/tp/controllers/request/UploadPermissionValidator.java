package com.tdd.tp.controller;

import javax.validation.constraints.NotBlank;

public class UploadPermissionValidator {

	@NotBlank(message="El token es requerido")
	private String token;

	public String getToken(){
		return this.token;
	}
}
