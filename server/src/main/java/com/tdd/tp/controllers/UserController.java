package com.tdd.tp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import com.tdd.tp.models.User;
import com.tdd.tp.repositories.UserRepository;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class UserController {

	@Autowired
	UserRepository userRepository;

	//@RequestMapping(value={"/login"}, method = RequestMethod.POST)
	//public String login(@RequestBody LoginRequest loginRequest) {
	//	return loginRequest.getEmail();
	//}
	/*@Autowired
	private PasswordEncoder passwordEncoder;*/

	@RequestMapping(value={"/login"}, method = RequestMethod.POST)
	public ResponseEntity login(@RequestBody LoginRequest loginRequest){
		try {
			/*User user = new User(
				loginRequest.getEmail(),
				"",
				"",
				"",
				loginRequest.getPass()
			);*/
			return new ResponseEntity<>("", HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@RequestMapping(value={"/register"}, method = RequestMethod.POST)
	public ResponseEntity register(
			@RequestParam("email") String email, @RequestParam("password") String password,
			@RequestParam("name") String name, @RequestParam("lastname") String lastname,
			@RequestParam("birthdate") String birthdate, @RequestParam("avatar") MultipartFile avatar
		){
		try {
			/*User user = new User(
				email,
				password,//passwordEncoder.encode(registerRequest.getPassword()),
				name,
				lastname,
				birthdate
			);*/
			/*System.out.println("----------------------");
			System.out.println(passwordEncoder.encode(password));
			System.out.println(passwordEncoder.encode(password));
			System.out.println("----------------------");*/
			//User createdUser = userRepository.save(user);
			//System.out.println(createdUser);
			return new ResponseEntity<>("", HttpStatus.CREATED);
		}	catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}


