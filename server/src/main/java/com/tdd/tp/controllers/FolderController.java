package com.tdd.tp.controller;

import com.tdd.tp.models.User;
import com.tdd.tp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.beans.factory.annotation.Autowired;
import com.tdd.tp.models.Folder;
import com.tdd.tp.repositories.FolderRepository;
import com.tdd.tp.repositories.MediaRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.tdd.tp.security.JwtUtils;

@RestController
public class FolderController {

	@Autowired
	FolderRepository folderRepository;

	@Autowired
	MediaRepository mediaRepository;

	@Autowired
	JwtUtils jwtUtils;

	@RequestMapping(value={"/folders"}, method = RequestMethod.GET)
	public ResponseEntity getFolders(@RequestHeader("Authorization") String token){
		try
		{
			String userId = jwtUtils.getUserNameFromJwtToken((token.split(" "))[1]);
			HashMap<String, Object> response = new HashMap<String,Object>();
			List listOfChildFolders = new ArrayList<>();
			folderRepository.findByParentIdAndUserId("", userId).forEach(listOfChildFolders::add);
			List listOfChildMedias = new ArrayList<>();
			mediaRepository.findByParentIdAndUserId("", userId).forEach(listOfChildMedias::add);
			List listaVacia = new ArrayList<>();
			HashMap<String,Object> folder = new HashMap<String, Object>();
			folder.put("_id","");
			folder.put("name","root");
			folder.put("parentId","");
			folder.put("path",listaVacia);
			response.put("folder_childs",listOfChildFolders);
			response.put("media_childs",listOfChildMedias);
			response.put("folder", folder);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/folders/{folderId}"}, method = RequestMethod.GET)
	public ResponseEntity getFolder(@PathVariable String folderId, @RequestHeader("Authorization") String token){
		try
		{
			String userId = jwtUtils.getUserNameFromJwtToken((token.split(" "))[1]);
			HashMap<String, Object> response = new HashMap<String,Object>();
			List listOfChildFolders = new ArrayList<>();
			folderRepository.findByParentIdAndUserId(folderId, userId).forEach(listOfChildFolders::add);
			List listOfChildMedias = new ArrayList<>();
			mediaRepository.findByParentIdAndUserId(folderId, userId).forEach(listOfChildMedias::add);
			Folder folder = folderRepository.findById(folderId).get();
			response.put("folder_childs",listOfChildFolders);
			response.put("media_childs",listOfChildMedias);
			response.put("folder",folder);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/folders"}, method = RequestMethod.POST)
	public ResponseEntity createFolder(@RequestBody UploadFolderRequest uploadFolderRequest, @RequestHeader("Authorization") String token){
		try {
			if(uploadFolderRequest.getFolderName().equals("")  )
			{
				
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
			String userId = jwtUtils.getUserNameFromJwtToken((token.split(" "))[1]);
			HashMap<String,String> info = new HashMap<String,String>();
			List newPath = new ArrayList<>();
			if (!uploadFolderRequest.getParentId().isEmpty()){
				Folder parentFolder = folderRepository.findById(uploadFolderRequest.getParentId()).get();
					info.put("id",parentFolder.get_id());
					info.put("name",parentFolder.getName());
					parentFolder.getPath().forEach(newPath::add);
					newPath.add(info);
			}
			else{
				info.put("id","");
				info.put("name","root");
				newPath.add(info);
			}
			Folder folder = new Folder(
					uploadFolderRequest.getFolderName(),
					uploadFolderRequest.getParentId(),
					newPath,
					userId
			);
			Folder createdFolder = folderRepository.save(folder);
			return new ResponseEntity<>(createdFolder, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value={"/folders/{folderId}"}, method = RequestMethod.DELETE)
	public ResponseEntity deleteFolder(@PathVariable String folderId){
		try
		{
			Optional folder = folderRepository.findById(folderId);
			folderRepository.deleteById(folderId);
			return new ResponseEntity<>(folder, HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/folders/"}, method = RequestMethod.DELETE)
	public ResponseEntity deleteFolders(){
		try
		{
			folderRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}