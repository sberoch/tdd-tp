package com.tdd.tp.controller;

import javax.validation.constraints.NotBlank;

public class UploadPermissionRequest {

	@NotBlank(message="Archivo es requerido")
	@ExistingFile
	private String mediaId;

	@UserExists
	@NotBlank(message="Usuario es requerido")
	private String userId;

	@NotBlank(message="Codigo es requerido")
	private String code;

	public String getMediaId(){
		return this.mediaId;
	}

	public String getUserId(){
		return this.userId;
	}

	public String getCode(){
		return this.code;
	}
}
