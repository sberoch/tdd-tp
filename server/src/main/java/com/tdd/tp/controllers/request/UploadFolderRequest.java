package com.tdd.tp.controller;

import lombok.Data;

@Data
public class UploadFolderRequest {
	private String parentId;
	private String folderName;
}
