package com.tdd.tp.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.http.HttpStatus;
import com.tdd.tp.repositories.MediaGridRepository;

import com.tdd.tp.models.User;
import com.tdd.tp.repositories.UserRepository;
import com.tdd.tp.security.JwtUtils;
import com.tdd.tp.models.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	MediaGridRepository mediaGridRepository;

	@RequestMapping(value={"/login"}, method = RequestMethod.POST)
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		try {
			Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtUtils.generateJwtToken(authentication);
			
			UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

			return ResponseEntity.ok(new JwtResponse(
					jwt, 
				 	userDetails.getName(),
				 	userDetails.getEmail()
				)
			);
		}catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/authgoogle"}, method = RequestMethod.POST)
	public ResponseEntity<?> authenticateUserGoogle(@RequestBody AuthGoogleRequest authGoogleRequest) {
		try {
			if(!userRepository.existsByEmail(authGoogleRequest.getEmail())){
				User user = new User(
					authGoogleRequest.getEmail(),
					authGoogleRequest.getGivenName(),
					authGoogleRequest.getFamilyName(),
					"",
					encoder.encode(""),
					authGoogleRequest.getEmail()
				);
				userRepository.save(user);
			}
			User userRegister = userRepository.findById(authGoogleRequest.getEmail()).get();
			String password = "";
			if (encoder.matches("", userRegister.getPassword()))
				password = "";



			Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(userRegister.getEmail(), password));

			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtUtils.generateJwtToken(authentication);
			
			UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

			return ResponseEntity.ok(new JwtResponse(
					jwt, 
				 	userDetails.getName(),
				 	userDetails.getEmail()
				)
			);
		}catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/register"}, method = RequestMethod.POST)
	public ResponseEntity<?> registerUser(
			@RequestParam("email") String email, @RequestParam("password") String password,
			@RequestParam("name") String name, @RequestParam("lastname") String lastname,
			@RequestParam("birthdate") String birthdate, @RequestParam("avatar") MultipartFile avatar
		) {
		try {
			if (userRepository.existsByUsername(email)) {
				return ResponseEntity
						.badRequest()
						.body(new MessageResponse("Error: El email esta en uso!"));
			}

			User user = new User(
				email,
				name,
				lastname,
				birthdate,
				encoder.encode(password),
				email
			);

			userRepository.save(user);

			mediaGridRepository.addPhoto(email, avatar.getInputStream());

			Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(email, password));

			SecurityContextHolder.getContext().setAuthentication(authentication);
			String jwt = jwtUtils.generateJwtToken(authentication);
			
			UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

			return ResponseEntity.ok(new JwtResponse(
					jwt, 
				 	name,
				 	email
				)
			);
		}catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}