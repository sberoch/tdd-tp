package com.tdd.tp.controller;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class RegisterRequest {
	private String email;
	private String name;
	private String lastname;
	private String birthdate;
	private String password;
}