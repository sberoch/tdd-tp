package com.tdd.tp.controller;

import com.tdd.tp.models.User;
import com.tdd.tp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.beans.factory.annotation.Autowired;
import com.tdd.tp.models.Media;
import com.tdd.tp.models.Folder;
import com.tdd.tp.repositories.FolderRepository;
import com.tdd.tp.repositories.MediaRepository;
import com.tdd.tp.repositories.MediaGridRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.HashMap;
import java.io.File;

import java.io.FileInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.*;

import org.springframework.web.multipart.MultipartFile;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import com.tdd.tp.security.JwtUtils;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
public class MediaController {

	@Autowired
	MediaGridRepository mediaGridRepository;

	@Autowired
	MediaRepository mediaRepository;

	@Autowired
	FolderRepository folderRepository;

	@Autowired
	JwtUtils jwtUtils;

	@RequestMapping(value={"/medias"}, method = RequestMethod.GET)
	public ResponseEntity getMedias(){
		try
		{
			List listOfMedias = new ArrayList<>();
			mediaRepository.findAll().forEach(listOfMedias::add);
			return new ResponseEntity<>(listOfMedias, HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    
    @RequestMapping(value={"/medias/{mediaId}"}, method = RequestMethod.GET)
	public ResponseEntity getMedia(@PathVariable String mediaId){
		try
		{
			InputStream file = mediaGridRepository.obtenerFile(mediaId);
			Media media = mediaRepository.findById(mediaId).get();
			Media createdMedia = new Media(
			 media.getName(),
			 media.getParentId(),
			 media.getUserId()
			 );
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      int data = file.read();
      while (data >= 0) {
					outputStream.write((char) data);
					data = file.read();
			}
			byte[] bytesToWriteTo = outputStream.toByteArray();
			file.close();
			HashMap<String, Object> response = new HashMap<String,Object>();
			response.put("metadata", createdMedia);
			response.put("file", bytesToWriteTo);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@RequestMapping(value={"/medias"}, method = RequestMethod.POST)
	public ResponseEntity createMedia(@RequestParam("file") MultipartFile file,
	 @RequestParam("parentId") String parentId, @RequestParam("fileName") String fileName, @RequestHeader("Authorization") String token){
		try {

			String userId = jwtUtils.getUserNameFromJwtToken((token.split(" "))[1]);
			if (folderRepository.findById(parentId).isEmpty()){
				Media media = new Media(
				fileName,
				"",
				userId
				);
				Media createdMedia = mediaRepository.save(media);
				mediaGridRepository.add(createdMedia.get_id(),file.getInputStream());
				return new ResponseEntity<>(createdMedia, HttpStatus.CREATED);
			}else{ 
				Media media = new Media(
				fileName,
				parentId,
				userId
				);
				Media createdMedia = mediaRepository.save(media);
				mediaGridRepository.add(createdMedia.get_id(),file.getInputStream());
				return new ResponseEntity<>(createdMedia, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@RequestMapping(value={"/medias/{mediaId}"}, method = RequestMethod.DELETE)
	public ResponseEntity deleteMedia(@PathVariable String mediaId){
		try
		{
			Optional media = mediaRepository.findById(mediaId);
			mediaRepository.deleteById(mediaId);
			mediaGridRepository.deleteFile(mediaId);
			return new ResponseEntity<>(media, HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value={"/medias/"}, method = RequestMethod.DELETE)
	public ResponseEntity deleteMedias(){
		try
		{
			mediaRepository.deleteAll();
			mediaGridRepository.deleteAllFiles();
			return new ResponseEntity<>(HttpStatus.OK);
		}
		catch (Exception e)
		{
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
