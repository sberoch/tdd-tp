package com.tdd.tp.controller;

import java.util.List;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private String email;
	private String name;

	public JwtResponse(String accessToken, String name, String email) {
		this.token = accessToken;
		this.name = name;
		this.email = email;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}