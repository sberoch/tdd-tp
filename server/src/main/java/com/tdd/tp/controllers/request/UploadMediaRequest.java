package com.tdd.tp.controller;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UploadMediaRequest {
	private String parentId;
	private String fileName;
	private MultipartFile file;
}
