package com.tdd.tp.controller;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.tdd.tp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class UserExistsValidator implements ConstraintValidator<UserExists, String> {

	@Autowired
	UserRepository userRepository;

  public void initialize(ExistingFile user_id) {
  }

  @Override
  public boolean isValid(String user_id, ConstraintValidatorContext cxt) {
    return !userRepository.findById(user_id).isEmpty();
  }

}