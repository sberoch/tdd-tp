package com.tdd.tp.controller;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.tdd.tp.repositories.MediaRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class ExistingFileValidator implements ConstraintValidator<ExistingFile, String> {

	@Autowired
	MediaRepository mediaRepository;

  public void initialize(ExistingFile file_id) {
  }

  @Override
  public boolean isValid(String file_id, ConstraintValidatorContext cxt) {
    return !mediaRepository.findById(file_id).isEmpty();
  }

}