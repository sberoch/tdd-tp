package com.tdd.tp.controller;

import lombok.Data;

@Data
public class AuthGoogleRequest {
	private String email;
	private String familyName;
	private String givenName;
	private String googleId;
	private String imageUrl;
	private String name;
}