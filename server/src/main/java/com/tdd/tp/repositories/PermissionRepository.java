package com.tdd.tp.repositories;

import com.tdd.tp.models.Permission;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PermissionRepository extends MongoRepository<Permission, String> {
	List findByUserId(String userId);
	List findByUserIdAndValidate(String userId, String validate);
	List findByMediaId(String mediaId);
	List findByMediaIdAndValidate(String mediaId, String validate);
	List findByUserIdAndMediaId(String userId, String mediaId);
	Boolean existsByUserIdAndMediaIdAndValidate(String userId, String mediaId, String validate);
}