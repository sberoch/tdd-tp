package com.tdd.tp.repositories;

import com.tdd.tp.models.Folder;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FolderRepository extends MongoRepository<Folder, String> {
    List findByParentId(String parentId);
    List findByParentIdAndUserId(String parentId, String userId);
}