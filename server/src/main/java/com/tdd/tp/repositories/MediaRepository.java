package com.tdd.tp.repositories;

import com.tdd.tp.models.Media;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MediaRepository extends MongoRepository<Media, String> {
    List findByParentId(String parentId);
    List findByParentIdAndUserId(String parentId, String userId);
}
