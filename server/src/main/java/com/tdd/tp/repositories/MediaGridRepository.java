package com.tdd.tp.repositories;

import com.tdd.tp.models.Media;
import org.springframework.data.mongodb.repository.MongoRepository;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.gridfs.GridFsOperations;


import com.mongodb.client.gridfs.model.GridFSFile;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.bson.types.ObjectId;

import org.springframework.web.multipart.MultipartFile;

import org.bson.BasicBSONObject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import org.springframework.stereotype.Service;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Criteria;

@Service
public class MediaGridRepository {

    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Autowired
    private GridFsOperations operations;

    @Autowired
    private GridFsTemplate gridFsTemplatePhoto;

    public String add(String title, InputStream file) throws IOException { 
       ObjectId id = gridFsTemplate.store(
         file, title);
       return id.toString(); 
    }

    public InputStream obtenerFile(String name) throws IllegalStateException, IOException { 
        InputStream  file = gridFsTemplate.getResource(name).getInputStream();
        return file; 
    }

    public void deleteFile(String identificador){
    	Query query = new Query (Criteria.where("filename").is(identificador));
		this.gridFsTemplate.delete(query);
	}

	public void deleteAllFiles(){
		this.gridFsTemplate.delete(new Query());
	}
	
    public String addPhoto(String title, InputStream file) throws IOException { 
       ObjectId id = gridFsTemplatePhoto.store(
         file, title);
       return id.toString(); 
    }

    public InputStream obtenerPhoto(String name) throws IllegalStateException, IOException { 
        InputStream  file = gridFsTemplatePhoto.getResource(name).getInputStream();
        return file; 
    }

    public void deletePhoto(String identificador){
        Query query = new Query (Criteria.where("filename").is(identificador));
        this.gridFsTemplate.delete(query);
    }
}